﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itemmap : MonoBehaviour
{
    public GameObject itemM;
    public bool inTrigger;




    void OnTriggerEnter(Collider other)
    {
        inTrigger = true;
    }

    void OnTriggerExit(Collider other)
    {
        inTrigger = false;
    }




    void Update()
    {
        if (inTrigger)
        {

            itemM.SetActive(true);

        }
        else { itemM.SetActive(false); }
    }
}
