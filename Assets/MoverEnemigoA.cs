﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoverEnemigoA : MonoBehaviour

   
{

    float mov = 0;
    Vector3 posini;

    // Start is called before the first frame update
    void Start()
    {

        posini = this.gameObject.transform.position;
        Destroy(this.gameObject,9f);
        
    }

    // Update is called once per frame
    void Update()
    {
        this.gameObject.transform.position = new Vector3(posini.x, posini.y, posini.z + mov);
        mov = mov - 0.2f;
    }
}
