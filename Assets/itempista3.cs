﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itempista3 : MonoBehaviour
{
    public GameObject item3;
    public bool inTrigger;




    void OnTriggerEnter(Collider other)
    {
        inTrigger = true;
    }

    void OnTriggerExit(Collider other)
    {
        inTrigger = false;
    }




    void Update()
    {
        if (inTrigger)
        {

            item3.SetActive(true);

        }
        else { item3.SetActive(false); }
    }
}

