﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Palanca : MonoBehaviour
{
    public bool inTrigger;
    public Texture crow;
    public GameObject scream2;
    public GameObject item;
    public GameObject Crow;



    void OnTriggerEnter(Collider other)
    {
        inTrigger = true;
    }

    void OnTriggerExit(Collider other)
    {
        inTrigger = false;
    }






    void Update()
    {

        if (inTrigger)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                DoorPista.doorKey = true;
                Crow.SetActive(true);
                scream2.SetActive(true);
                Destroy(item);
                Destroy(this.gameObject);

            }

        }
    }

    void OnGUI()
    {
        if (inTrigger)
        {
            GUI.DrawTexture(new Rect(Screen.width / 2, Screen.height / 2, 80, 80), crow);
        }
    }
}