﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itemDoll : MonoBehaviour
{
    public GameObject itemD;
    public bool inTrigger;




    void OnTriggerEnter(Collider other)
    {
        inTrigger = true;
    }

    void OnTriggerExit(Collider other)
    {
        inTrigger = false;
    }




    void Update()
    {
        if (inTrigger)
        {

            itemD.SetActive(true);

        }
        else { itemD.SetActive(false); }
    }
}
