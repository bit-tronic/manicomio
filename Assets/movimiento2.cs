﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimiento2 : MonoBehaviour
{
    public Texture take;
    float mov1 = 0;
    Vector3 posini;
    public GameObject OBJ;
    public GameObject sound;

    public bool inTrigger;

    void OnTriggerEnter(Collider other)
    {
        inTrigger = true;
       

    }

    void OnGUI()
    {

        if (inTrigger)
        {


            GUI.DrawTexture(new Rect(Screen.width / 2, Screen.height / 2, 35, 50), take);
        }  
    }
    void OnTriggerExit(Collider other)
    {
        inTrigger = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        posini = OBJ.transform.position;
    }

    // Update is called once per frame
    void Update()
    {


        if (inTrigger)
        {

            if (Input.GetKeyDown(KeyCode.E))
            {
                sound.SetActive(true);
                Rigidbody rb = OBJ.GetComponent<Rigidbody>();
                rb.AddForce(0, 0, 11, ForceMode.Impulse);
                Destroy(this.gameObject);
            }
        }
    }
}