﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itemcrow : MonoBehaviour
{
    public GameObject itemC;
    public bool inTrigger;




    void OnTriggerEnter(Collider other)
    {
        inTrigger = true;
    }

    void OnTriggerExit(Collider other)
    {
        inTrigger = false;
    }




    void Update()
    {
        if (inTrigger)
        {

            itemC.SetActive(true);

        }
        else { itemC.SetActive(false); }
    }
}
