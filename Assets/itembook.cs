﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itembook : MonoBehaviour
{

    public GameObject itemB;
    public GameObject Book;
    public bool inTrigger;




    void OnTriggerEnter(Collider other)
    {
        inTrigger = true;
    }

    void OnTriggerExit(Collider other)
    {
        inTrigger = false;
    }




    void Update()
    {
        if (inTrigger)
        {

            itemB.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E)) { Destroy(Book); }

        }
        else { itemB.SetActive(false); }
    }
}