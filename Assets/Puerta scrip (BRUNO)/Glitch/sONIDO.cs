﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sONIDO : MonoBehaviour
{

    AudioClip sound;
    public bool inTrigger;

    void OnTriggerEnter(Collider other)
    {
        inTrigger = true;
    }

    void OnTriggerExit(Collider other)
    {
        inTrigger = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (inTrigger)
        {

            GetComponent<AudioSource>().Play();
        }
        else { GetComponent<AudioSource>().Stop(); }
    }
}
