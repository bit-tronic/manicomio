﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cambiar : MonoBehaviour
{

    public Glitch glitch;
    public bool inTrigger; 

    void OnTriggerEnter(Collider other)
    {
        inTrigger = true;
    }

    void OnTriggerExit(Collider other)
    {
        inTrigger = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (inTrigger)
        {
            glitch.enabled = true;
        }
        else {
            glitch.enabled = false;
        }
    
    }
}
