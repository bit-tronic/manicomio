﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class puerta : MonoBehaviour
{
    public bool inTrigger;
    public GameObject scream;
    public GameObject item;
    public Texture take;
    public GameObject sound;
    public GameObject MenuL;
    
    

    void OnTriggerEnter(Collider other)
    {
        inTrigger = true;
    }

    void OnTriggerExit(Collider other)
    {
        inTrigger = false;
    }






    void Update()
    {
        
       if (inTrigger)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {

                DoorScript.doorKey = true;
                sound.SetActive(true);
                MenuL.SetActive(true);
                scream.SetActive(true);
                Destroy(item);
                Destroy(this.gameObject);

            }
           
        }
    }

    void OnGUI()
    {
        if (inTrigger)
        {

            GUI.DrawTexture(new Rect(Screen.width / 2, Screen.height / 2, 35, 50), take);
        }
    }
}