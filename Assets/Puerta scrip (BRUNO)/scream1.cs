﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scream1 : MonoBehaviour
{
    public GameObject Enemigo;
    public bool open;
    public bool close;
    public bool inTrigger;    

    void OnTriggerEnter(Collider other)
    {        
        inTrigger = true;
    }

    void OnTriggerExit(Collider other)
    {
        inTrigger = false;
    }

    void Update()
    {
        if (inTrigger)
        {
            Vector3 pos = new Vector3(this.gameObject.transform.position.x + 5, 5, this.gameObject.transform.position.z+2);
             GameObject enem = Instantiate(Enemigo,pos, Quaternion.identity);
             enem.transform.rotation = Quaternion.Euler(0,180,0);
            Destroy(this.gameObject);
           
        }
    }
}
