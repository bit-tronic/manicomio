﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scream2 : MonoBehaviour
{
    public GameObject pelota;
    public bool open;
    public bool close;
    public bool inTrigger;

    void OnTriggerEnter(Collider other)
    {
        inTrigger = true;
    }

    void OnTriggerExit(Collider other)
    {
        inTrigger = false;
    }

    void Update()
    {
        if (inTrigger)
        {
            Vector3 pos = new Vector3(this.gameObject.transform.position.x - 14, 1, this.gameObject.transform.position.z - 2 );
            GameObject pel = Instantiate(pelota, pos, Quaternion.identity);
            pel.transform.rotation = Quaternion.Euler(0, 0, 0);
            Destroy(this.gameObject);

        }
    }
}

