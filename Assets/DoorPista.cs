﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorPista : MonoBehaviour
{
    public Texture ope;
    public Texture clos;
    public Texture bloq;
    public static bool doorKey;
    public bool open;
    public bool close;
    public bool inTrigger;

    void OnTriggerEnter(Collider other)
    {
        inTrigger = true;
    }

    void OnTriggerExit(Collider other)
    {
        inTrigger = false;
    }

    void Update()
    {
        if (inTrigger)
        {
            if (close)
            {
                if (doorKey)
                {
                    if (Input.GetKeyDown(KeyCode.E) || Input.GetKey(KeyCode.JoystickButton2))
                    {
                        open = true;
                        close = false;
                    }
                }
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.E) || Input.GetKey(KeyCode.JoystickButton2))
                {
                    close = true;
                    open = false;
                }
            }
        }

        if (open)
        {
            var newRot = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0.0f, 90.0f, 0.0f), Time.deltaTime * 200);
            transform.rotation = newRot;
        }
        else
        {
            var newRot = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0.0f, 180.0f, 0.0f), Time.deltaTime * 200);
            transform.rotation = newRot;
        }
    }

    void OnGUI()
    {
        if (inTrigger)
        {
            if (open)
            {
                GUI.DrawTexture(new Rect(Screen.width / 2, Screen.height / 2, 35, 50), clos);
            }
            else
            {
                if (doorKey)
                {

                    GUI.DrawTexture(new Rect(Screen.width / 2, Screen.height / 2, 35, 50), ope);
                }
                else
                {

                    GUI.DrawTexture(new Rect(Screen.width / 2, Screen.height / 2, 50, 50), bloq);
                }
            }
        }
    }
}
