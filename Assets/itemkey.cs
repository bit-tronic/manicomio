﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itemkey : MonoBehaviour
{
    public GameObject itemK;
    public bool inTrigger;




    void OnTriggerEnter(Collider other)
    {
        inTrigger = true;
    }

    void OnTriggerExit(Collider other)
    {
        inTrigger = false;
    }




    void Update()
    {
        if (inTrigger)
        {

            itemK.SetActive(true);

        }
        else { itemK.SetActive(false); }
    }
}
