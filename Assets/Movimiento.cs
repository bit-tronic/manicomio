﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento : MonoBehaviour
{
    public GameObject sound;
    float mov1 = 0;
    Vector3 posini;
    public GameObject Silla;
    
    public bool inTrigger;

    void OnTriggerEnter(Collider other)
    {
        inTrigger = true;
        sound.SetActive(true);
       Rigidbody rb = Silla.GetComponent<Rigidbody>();
       rb.AddForce(0,0,10,ForceMode.Impulse);
    }




    void OnTriggerExit(Collider other)
    {
        //inTrigger = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        posini = Silla.transform.position;
    }

    // Update is called once per frame
    void Update()
    {


        if (inTrigger)
        {
            Destroy(this.gameObject);
        }
    }
}
